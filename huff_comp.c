#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct codeword {
        char c;
        unsigned char * word;
        int len;
        int count;
} codeword;

FILE * outf;

//Expect 1<=num<=8 bits
void writebits(unsigned char bits,int num){
        static int rem=0;
        static unsigned char buf=0;
        printf("%d ",(buf^(bits>>rem))==(buf|(bits>>rem)));
        buf^=(unsigned char)(bits>>rem);
        if(num+rem>=8){
                fputc(buf,outf);
                //for(int i=0;i<8;i++)
                //        printf("%d",1&(buf>>(7-i)));
                //putchar(' ');
                buf=bits<<(8-rem);
        }
        rem=(num+rem)%8;
}

int maxprobcw(const void * a,const void * b){
        return ((codeword *)a)->count>((codeword *)b)->count?1:-1;
}

int origcw(const void * a,const void * b){
        return ((unsigned char)((codeword *)a)->c)
                >((unsigned char)((codeword *)b)->c)
                ?1:-1;
}

void findhuffw(codeword * cws,int n){
        //Nothing to do if one word only
        if(n<2)return;
        qsort(cws,n,sizeof(codeword),maxprobcw);

        //Solve the modified subproblem specified by algorithm
        char char_second_least=cws[1].c;
        cws[1].count+=cws[0].count;
        findhuffw(cws+1,n-1);

        int position_second_least;
        for(position_second_least=0;
        cws[position_second_least].c!=char_second_least;
        position_second_least++);

        cws[position_second_least].count-=cws[0].count;
        cws[0].word=calloc((1+cws[position_second_least].len/8),sizeof(char));
        if(cws[0].word==NULL){
                printf("Call to calloc() returned NULL!\n");
                exit(EXIT_FAILURE);
        }

        cws[0].len=cws[position_second_least].len;

        if(!(cws[0].len%8)){
                cws[0].word=realloc(
                        cws[0].word,
                        (cws[0].len/8+1)*sizeof(char));
                if(cws[0].word==NULL){
                        printf("Call to realloc() returned NULL!\n");
                        exit(EXIT_FAILURE);
                }
                cws[0].word[cws[0].len/8]=(char)0;

                cws[position_second_least].word=realloc(
                        cws[position_second_least].word,
                        (cws[0].len/8+1)*sizeof(char));
                if(cws[position_second_least].word==NULL){
                        printf("Call to realloc() returned NULL!\n");
                        exit(EXIT_FAILURE);
                }
        }

        memcpy(cws[0].word,
                cws[position_second_least].word,
                cws[position_second_least].len/8+1);

        cws[0].word[cws[0].len/8]^=1<<(7-(cws[0].len%8));
        cws[0].len++;
        cws[position_second_least].len++;
}
 
int main(int argc, char ** argv){
        if(argc!=2){
                printf("Usage: %s <filename>\n",argv[0]);
                exit(EXIT_FAILURE);
        }

        FILE * f=fopen(argv[1],"r");
        if(f==NULL){
                printf("Opening file %s failed!\n",argv[1]);
                exit(EXIT_FAILURE);
        }

        int c;
        int flen=0;
        codeword cwords[256]={0};

        while((c=fgetc(f))!=EOF){
                cwords[c].count++;
                flen++;
        }

        if(!flen){
                printf("Empty file!\n");
                exit(EXIT_FAILURE);
        }

        for(int i=0;i<256;i++)
                cwords[i].c=(char)i;

        qsort(cwords,256,sizeof(codeword),maxprobcw);
        int numdist,i;
        for(i=0;cwords[i].count==0;i++);
        numdist=256-i;
        codeword * cwordstart=cwords+i;
        findhuffw(cwordstart,numdist);

        int namlen=strlen(argv[1]);
        char * oname=malloc(namlen+6);
        oname[namlen+5]='\0';
        strcpy(oname,argv[1]);
        strcpy(oname+namlen,".huff");
        outf=fopen(oname,"w+");
        if(outf==NULL){
                printf("Opening file %s failed!\n",oname);
                exit(EXIT_FAILURE);
        }
        fwrite(&numdist,sizeof(int),1,outf);
        fwrite(&flen,sizeof(int),1,outf);
        for(int i=0;i<numdist;i++){
                fwrite(&cwordstart[i].c,sizeof(char),1,outf);
                fwrite(&cwordstart[i].len,sizeof(int),1,outf);
                fwrite(cwordstart[i].word,1,(cwordstart[i].len+7)/8,outf);
                printf("\n%c %d ",cwordstart[i].c,cwordstart[i].len);
                for(int j=0;j<cwordstart[i].len;j++)
                        printf("%d",1&(cwordstart[i].word[j/8]>>(7-(j%8))));
                putchar('\n');
        }

        rewind(f);

        qsort(cwords,256,sizeof(codeword),origcw);
        while((c=fgetc(f))!=EOF)
                for(int i=0,
                        l=cwords[c].len;
                        l>0;
                        i++,l-=8)
                        writebits(cwords[c].word[i],
                                (l>8)?8:l);

        //Write seven bits to force another byte write if necessary
        writebits(0,7);

        fclose(f);
        fclose(outf);

}
