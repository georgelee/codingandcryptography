#include<stdio.h>
#include<stdlib.h>
#include<string.h>

FILE * f;
int getbit(){
        static int bitsread=0;
        static char buf;
        int i;
        if(!(bitsread%8)){
                buf=i=fgetc(f);
                if(i==EOF){
                        printf("\nUnexpectedly reached EOF!\n");
                        exit(-1);
                }
        }
        //printf("\n%d",1&(buf>>(7-(bitsread++%8))));
        return 1&(buf>>(7-(bitsread++%8)));
}

typedef struct bint{
        struct bint * ch;
        char val;
        char isch;
} bint;

int main(int argc,char ** argv){
        FILE * outf=stdout;
        if(argc!=2){
                if(argc!=3){
                        printf("Usage: %s <filename>\n",argv[1]);
                        printf("Usage: %s <filename> <output file>\n",
                                argv[1]);
                        exit(EXIT_FAILURE);
                }else{
                        outf=fopen(argv[2],"w+");
                }
        }
        f=fopen(argv[1],"r+");
        if(f==NULL){
                printf("Opening file %s failed!\n",argv[1]);
                exit(-1);
        }
        
        int numdist,flen;
        fread(&numdist,sizeof(int),1,f);
        fread(&flen,sizeof(int),1,f);
        if(numdist>256){
                printf("Invalid input file!\n");
                exit(EXIT_FAILURE);
        }

        char ch;
        int len;
        char * word;
        int bitsread=0;

        bint * head=calloc(1,sizeof(bint));
        bint * curr;
        for(int i=0;i<numdist;i++){
                fread(&ch,sizeof(char),1,f);
                fread(&len,sizeof(int),1,f);
                if(len>256){
                        printf("Invalid input file!\n");
                        exit(EXIT_FAILURE);
                }
                word=calloc(((len+7)/8),sizeof(char));
                if(word==NULL){
                        printf("Call to calloc() returned NULL!\n");
                        exit(-1);
                }
                fread(word,sizeof(char),(len+7)/8,f);
                curr=head;
                for(int j=0;j<len;j++){
                        if(!curr->ch){
                                curr->ch=calloc(2,sizeof(bint));
                                if(curr->ch==NULL){
                                        printf("Call to calloc() "
                                                "returned NULL!\n");
                                        exit(-1);
                                }
                        }
                        curr=curr->ch+(1&(word[j/8]>>(7-(j%8))));
                }
                curr->val=ch;
                curr->isch=1;
                free(word);
        }
        for(int i=0;i<flen;i++){
                curr=head;
                while(!(curr->isch))
                        curr=curr->ch+getbit();
                fputc(curr->val,outf);
        }
}
